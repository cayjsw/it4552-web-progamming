<?php
$str = "";
$result = "";
if (isset($_POST['option'])) {
    $str = $_POST['src_string'];
    $opt = $_POST['option'];

    switch ($opt) {
        case 'strlen':
            $result = strlen($str);
            break;
        case 'strtolower':
            $result = strtolower($str);
            break;
        case 'trim':
            $result = trim($str);
            break;
        case 'strtoupper':
            $result = strtoupper($str);
            break;
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <title>String Handle</title>
</head>

<body>
    <div class="container">
        <form action="" method="post">
            <!-- string input -->
            <div class="row">
                <label for="src_string" class="col-form-label">String:</label>
                <div class="col-3">
                    <input type="text" class="form-control" name="src_string" id="src_string" value="<?php echo $str; ?>">
                </div>
            </div>

            <!-- radio option -->
            <dic class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="option" id="strlen-opt" value="strlen">
                    <label class="form-check-label" for="strlen-opt">strlen</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="option" id="tolower-opt" value="strtolower">
                    <label class="form-check-label" for="tolower-opt">strtolower</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="option" id="trim-opt" value="trim">
                    <label class="form-check-label" for="trim-opt">trim</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="option" id="toupper-opt" value="strtoupper">
                    <label class="form-check-label" for="toupper-opt">strtoupper</label>
                </div>
            </dic>

            <button class="btn btn-primary">Submit</button>

            <!-- result -->
            <div class="row">
                <label for="result" class="col-form-label col-1">Result:</label>
                <div class="col-5">
                    <input type="text" class="form-control" name="" id="result" value="<?php echo $result ?>" disabled>
                </div>
            </div>
        </form>
    </div>
</body>

</html>