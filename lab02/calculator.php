<?php

$num1 = "";
$num2 = "";
$result = "";

if (isset($_POST['operation'])) {
    $num1 = $_POST['firstNum'];
    $num2 = $_POST['secNum'];
    $op = $_POST['operation'];

    if (is_numeric($num1) && is_numeric($num2)) {
        switch ($op) {
            case 'add':
                $result = $num1 + $num2;
                break;
            case 'sub':
                $result = $num1 - $num2;
                break;
            case 'pro':
                $result = $num1 * $num2;
                break;
            case 'divide':
                $result = $num1 / $num2;
                break;
        }
    }
    
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <title>Calculator</title>
</head>

<body>
    <div class="container">
        <form action="" method="post">
            <!-- first number -->
            <div class="row">
                <label for="firstNum" class="col-form-label">First number</label>
                <div class="col-3">
                    <input type="number" class="form-control" name="firstNum" id="firstNum" value="<?php echo $num1;?>">
                </div>
            </div>

            <!-- operation -->
            <dic class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="operation" id="add" value="add">
                    <label class="form-check-label" for="add">+</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="operation" id="sub" value="sub">
                    <label class="form-check-label" for="sub">-</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="operation" id="pro" value="pro">
                    <label class="form-check-label" for="pro">x</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="operation" id="divide" value="divide">
                    <label class="form-check-label" for="divide">/</label>
                </div>
            </dic>

            <!-- second number -->
            <div class="row">
                <label for="secNum" class="col-form-label">Second number</label>
                <div class="col-3">
                    <input type="number" class="form-control" name="secNum" id="secNum" value="<?php echo $num2;?>">
                </div>
            </div>

            <button class="btn btn-primary">Submit</button>

            <!-- result -->
            <div class="row">
                <label for="result" class="col-form-label col-1">Result:</label>
                <div class="col-1">
                    <input type="text" class="form-control" name="" id="result" value="<?php echo $result ?>" disabled>
                </div>
            </div>
        </form>
    </div>
</body>

</html>