<html>

<body>
    <?php
    function user_sort($a, $b)
    {
        // smarts is all-important, so sort it first 
        if ($b == 'smarts') {
            return 1;
        } else if ($a == 'smarts') {
            return -1;
        }
        return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
    }
    $values = array(
        'name' => 'Buzz Lightyear',

        'email_address' => 'buzz@starcommand.gal',

        'age' => 32,

        'smarts' => 'some'
    );
    function print_checked($sort_type)
    {
        if (isset($_POST["sort_type"])) {
            if ($_POST["sort_type"] == $sort_type)
                echo "checked";
        }
    }
    ?>
    <form action="UserSorting.php" method="post">
        <p>
            <input type="radio" name="sort_type" value="sort" <?php print_checked("sort"); ?> /> Standard sort<br />
            <input type="radio" name="sort_type" value="rsort" <?php print_checked("rsort"); ?> /> Reverse sort<br />
            <input type="radio" name="sort_type" value="usort" <?php print_checked("usort"); ?> /> User-defined sort<br />
            <input type="radio" name="sort_type" value="ksort" <?php print_checked("ksort"); ?> /> Key sort<br />
            <input type="radio" name="sort_type" value="krsort" <?php print_checked("krsort"); ?> /> Reverse key sort<br />
            <input type="radio" name="sort_type" value="uksort" <?php print_checked("uksort"); ?> /> User-defined key sort<br />
            <input type="radio" name="sort_type" value="asort" <?php print_checked("asort"); ?> /> Value sort<br />
            <input type="radio" name="sort_type" value="arsort" <?php print_checked("arsort"); ?> /> Reverse value sort<br />
            <input type="radio" name="sort_type" value="uasort" <?php print_checked("uasort"); ?> /> User-defined value sort<br />
        </p>
        <input type="submit" value="Sort" name="submitted" />
        <p>
            Values unsorted:
        </p>
        <ul>
            <?php
            foreach ($values as $key => $value) {
                echo "<li><b>$key</b>: $value</li>";
            }
            ?>
        </ul>

        <?php
        if (isset($_POST['submitted'])) {
            $submitted = $_POST['submitted'];
            $sort_type = $_POST['sort_type'];
            if ($sort_type == 'usort' || $sort_type == 'uksort' || $sort_type == 'uasort') {
                $sort_type($values, 'user_sort');
            } else {
                $sort_type($values);
            }
        }
        ?>
        <p>
            <?php if (isset($submitted)) {
                print " Values sorted by " . $sort_type;
            } ?>:
        </p>

        <ul>
            <?php
            if (isset($submitted)) {
                foreach ($values as $key => $value) {
                    echo "<li><b>$key</b>: $value</li>";
                }
            }
            ?>
        </ul>
    </form>
</body>

</html>