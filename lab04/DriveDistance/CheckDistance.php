<html>
    <head>
        <title>Distance and Time Calculations</title>
    </head>
    <body>
        <?php
        if (isset($_POST['destination'])) {
            $destinations = $_POST['destination'];

            $cities = array('Dallas' => 803, 'Toronto' => 435, 'Boston' => 848,
                            'Nashville' => 406, 'Las Vegas' => 1526, 
                            'San Franciso' => 1835, 'Washington, DC' => 595,
                            'Miami' => 1189,'Pittsburgh' => 409);
            
            print "<table>
                    <tr>
                        <th>No.</th>
                        <th>Destination</th>
                        <th>Distance</th>
                        <th>Driving time</th>
                        <th>Walking time</th>
                    </tr>";
            foreach ($destinations as $key=>$destination) {
                $distance = $cities[$destination];
                $time = round(($distance / 60), 2);
                $walktime = round(($distance / 5), 2);
                print "<tr>
                        <td>$key</td>
                        <td>$destination</td>
                        <td>$distance</td>
                        <td>$time</td>
                        <td>$walktime</td>
                    </tr>";
            }
            print "</table>";
            }
            else {
                print "Sorry, have not choosen yet!.";
            }
        ?>
    </body>
</html>
<style>
    table, th, td {
        border:1px solid black;
    }
</style>