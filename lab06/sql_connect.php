<?php
require_once("define.php");

// create connection to database
$connect = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

// cho phep PHP luu unicode(uft8) -> database
mysqli_set_charset($connect, "utf8");

// kiem tra ket noi co thanh cong khong
if($connect->connect_error) {
    var_dump($connect->connect_error);
    die();
}
